﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMount : MonoBehaviour {

    public Transform CameraDollySlot;
    public float screenshake;
    private Transform CameraLerpTransform;
    private Vector3 CameraLerpPosition;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        CameraLerpTransform = CameraDollySlot;
        CameraLerpPosition = CameraLerpTransform.position;
        CameraLerpPosition += CameraLerpTransform.up *Random.Range(-screenshake, screenshake);
        CameraLerpPosition += CameraLerpTransform.right * Random.Range(-screenshake, screenshake);
        transform.position = Vector3.Lerp(transform.position, CameraLerpPosition, 0.5f);
        screenshake = Mathf.Lerp(screenshake, 0, 0.5f);
	}
}
