﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteAmmo : MonoBehaviour
{
    PickUpSystem sysRef = new PickUpSystem();
    public float decayTime = 30f;

    private void Update()
    {
        decayTime -= Time.deltaTime;
        if (decayTime <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("You touched an Infinite ammo pickup");

        if (other.tag == "Player")
        {
            Debug.Log("Player got Infinite Ammo");

            sysRef.InfiniteAmmo = true;

            Destroy(gameObject);
        }
    }


}