﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibleDrink : MonoBehaviour
{
    PickUpSystem sysRef = new PickUpSystem();
    public float decayTime = 30f;

    private void Update()
    {
        decayTime -= Time.deltaTime;
        if (decayTime <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("You touched a Invincible pickup");

        if (other.tag == "Player")
        {
            Debug.Log("Player got invincibility");

            sysRef.Invincibility = true;

            Destroy(gameObject);
        }
    }


}