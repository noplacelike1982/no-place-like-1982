﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PickUpSystem : MonoBehaviour
{
    private GameObject pickUpSystem;

    #region
    public float doubleTime = 12000f;
    public float tripleTime = 12000f;
    public float invicibleTime = 12000f;
    public float infiniteTime = 12000f;
    public float instaTime = 12000f;

    public Image doublePic;
    public Image triplePic;
    public Image invincePic;
    public Image infintePic;
    public Image instaPic;
    #endregion

    public GameObject[] cans;

    public float oddsOfCan = 20f;
    private float oddsRange;
    private int whichCan;

    public static bool _DoublePoints;
    public bool DoublePoints
    {
        get
        {
            return _DoublePoints;
        }
        set
        {
            _DoublePoints = value;
        }
    }
    public bool doublePoints;


    public static bool _TriplePoints;
    public bool TriplePoints
    {
        get
        {
            return _TriplePoints;
        }
        set
        {
            _TriplePoints = value;
        }
    }
    public bool triplePoints;

    public static bool _InfiniteAmmo;
    public bool InfiniteAmmo
    {
        get
        {
            return _InfiniteAmmo;
        }
        set
        {
            _InfiniteAmmo = value;
        }
    }
    public bool infiniteAmmo;

    public static bool _Invincibility;
    public bool Invincibility
    {
        get
        {
            return _Invincibility;
        }
        set
        {
            _Invincibility = value;
        }
    }
    public bool invincibility;

    public static bool _InstaKill;
    public bool InstaKill
    {
        get
        {
            return _InstaKill;
        }
        set
        {
            _InstaKill = value;
        }
    }
    public bool instaKill;

    private void Start()
    {
        //PickUpSystem pickUpSystem = new PickUpSystem();

    }

    private void Update()
    {
        doublePoints = _DoublePoints;
        triplePoints = _TriplePoints;
        infiniteAmmo = _InfiniteAmmo;
        invincibility = _Invincibility;
        instaKill = _InstaKill;

        if (doublePoints)
        {


            StartCoroutine("DoubleTimer");
        }

        if (triplePoints)
        {
            //triplePic.enabled = true;

            StartCoroutine("TripleTime");
        }

        if (invincibility)
        {
            //invincePic.enabled = true;

            StartCoroutine("InvincibilityTime");
        }

        if (infiniteAmmo)
        {
            //infintePic.enabled = true;

            StartCoroutine("InfiniteTime");
        }
        if (instaKill)
        {
            //instaPic.enabled = true;

            StartCoroutine("InstaTime");
        }


    }
    //random powerup generation
    public void MakeCan(Transform transform)
    {
        Debug.Log("MakeCan was called");
        oddsRange = Random.Range(0, 100f);
        //if oddsRange is less than the predetermined number then we make a new powerup
        if (oddsRange < oddsOfCan)
        {
            Debug.Log("Tried to make a can");
            whichCan = Random.Range(0, 3);

            Vector3 oldPosition = transform.position;
            oldPosition[2] += 5;

            float x = transform.position.x;
            float y = transform.position.y;
            float z = transform.position.z;

            GameObject power = Instantiate(cans[whichCan], new Vector3(x, ( y+ 1.2f ), z) , Quaternion.Euler(0, 45, 45)) as GameObject;

        }
    }


    IEnumerator DoubleTimer()
    {
        while (doubleTime > 0)
        {
            //doublePic.enabled = true;
            doubleTime -= 1;

            yield return new WaitForSecondsRealtime(doubleTime);
        }

        _DoublePoints = false;
        //doublePic.enabled = false;

    }

    IEnumerator TripleTime()
    {
        while (tripleTime > 0)
        {
            tripleTime -= 1;

            yield return new WaitForSecondsRealtime(tripleTime);
        }

        _TriplePoints = false;
        //triplePic.enabled = false;
    }

    IEnumerator InvincibilityTime()
    {
        while (invicibleTime > 0)
        {
            invicibleTime -= 1;

            yield return new WaitForSecondsRealtime(invicibleTime);
        }

        _Invincibility = false;
        //invincePic.enabled = false;
    }

    IEnumerator InfiniteTime()
    {
        while (infiniteTime > 0)
        {
            infiniteTime -= 1;

            yield return new WaitForSecondsRealtime(infiniteTime);
        }

        _InfiniteAmmo = false;
        //infintePic.enabled = false;
    }

    IEnumerator InstaTime()
    {
        while (instaTime > 0)
        {
            instaTime -= 1;

            yield return new WaitForSecondsRealtime(instaTime);
        }

        _InstaKill = false;
        //doublePic.enabled = false;
    }

}
