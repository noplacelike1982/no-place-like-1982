﻿using System.Collections;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    //spawning states
    public enum SpawnState { SPAWNING, WAITING, COUNTING };

    [System.Serializable]
    //wave constructor
    public class Wave
    {
        public string name;
        public Transform enemy;
        public int count;
        public float rate;
    }

    //array of waves called waves
    public Wave[] waves;
    private int nextWave = 0;

    public Transform[] spawnPoints;

    public float timeBetweenWaves = 5f;
    public float waveCountdown;

    // time between seaching for live enemies
    private float searchCountdown = 1f;

    private SpawnState state = SpawnState.COUNTING;

    void Start()
    {

        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points referenced");
        }

        //countdown between waves
        waveCountdown = timeBetweenWaves;
    }

    void Update()
    {
        if (state == SpawnState.WAITING)
        {
            if (!EnemyIsAlive())
            {
                //begin new round
                WaveCompleted();
            }
            else
            {
                return;
            }


        }
        //spawns a wave if the countdown is over, just one time
        if (waveCountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                // calls the IEnumerator funciton Spawnwave passing the next wave of enemies through the for loop
                StartCoroutine(SpawnWave(waves[nextWave]));
                FindObjectOfType<AudioManager>().Play("wave start");
                waveCountdown = 5f;
            }
        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }
    }

    void WaveCompleted()
    {
        Debug.Log("Wave Completed");
        FindObjectOfType<AudioManager>().Play("wave end");

        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if (nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
            Debug.Log("ALL WAVES COMPLETE. Looping");
        }
        else
        {
            nextWave++;
        }

    }

    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }
        return true;
    }

    //for loop that cycles through the waves array and spawns each enemy
    IEnumerator SpawnWave(Wave _wave)
    {
        Debug.Log("Spawning Wave" + _wave.name);
        state = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f / _wave.rate);
        }

        state = SpawnState.WAITING;

        yield break;
    }

    // function to instantiate enemies
    void SpawnEnemy(Transform _enemy)
    {
        //spawn enemies
        Debug.Log("Spawning Enemy");

        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(_enemy, _sp.position, _sp.rotation);

    }
}


