﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanRotate : MonoBehaviour {
    public float FanSpeed = 1.0f;
    // Use this for initialization
    void Start () {
        FanSpeed += Random.Range(-FanSpeed / 4, FanSpeed / 4);
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 0, FanSpeed * Time.deltaTime);
    }
}
