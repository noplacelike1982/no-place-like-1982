﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[HelpURL("http://example.com/docs/MyComponent.html")]
public class ThirdPersonCamera : MonoBehaviour {
    //Variable Declarations
    
    [Header("Object References")]
    public GameObject CameraSubject; //What is the Target Subject
    public GameObject CameraSubjectSpine; //What is the Target Subject's Spine
    public GameObject CameraDolly; //Reference to the Camera Dolly

    //Camera Offsets
    [Header("Camera Offsets")]
    [Range(-10.0f,10.0f)]
    public float CameraVerticalOffset = 0.0f;
    [Range(-10.0f, 10.0f)]
    public float CameraHorizontalOffset = 0.0f;
    [Range(-10.0f, 10.0f)]
    public float CameraForwardOffset = 0.0f;

    //Camera Offsets when Aiming Down Sight
    [Range(-10.0f, 10.0f)]
    public float CameraADSVerticalOffset = 0.0f;
    [Range(-10.0f, 10.0f)]
    public float CameraADSHorizontalOffset = 0.0f;
    [Range(-10.0f, 10.0f)]
    public float CameraADSForwardOffset = 0.0f;
    
    Vector3 FollowPosition;

    [Header("Camera Behavior Attributes")]
    public float CameraMovespeed = 120.0f; //How quickly the camera follows the Target Subject
    public float clampAngle = 80.0f;
    [Range(0.0f, 300.0f)]
    public float inputSensitivity = 150.0f;
    [Range(0.0f, 1.0f)]
    public float cameraADSLerp = 0.5f;

    [Header("Invert Camera Input")]
    public bool verticalInvert = false;
    public bool horizontalInvert = false;

    [Header("Camera Debug Mode")]
    public bool CameraDebugMode = false;
    [Range(0.0f, 1.0f)]
    public float CameraTargetGizmoSize = 1.0f;
    private float rotY = 0.0f;
    private float rotX = 0.0f;
    private float xAxisInput = 0.0f;
    private float yAxisInput = 0.0f;
    private float aimInput = 0.0f;

    private float CameraHOffset = 0.0f;
    private float CameraVOffset = 0.0f;
    private float CameraFOffset = 0.0f;

    //Components
    ThirdPersonCameraDolly CameraDollyComponent;

    // Use this for initialization
    void Start () {
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        CameraDollyComponent = CameraDolly.GetComponent<ThirdPersonCameraDolly>();
    }
	
	// Update is called once per frame
	void Update () {

        // Get The Player Input
        GetInput();

        //Set Rotations based on the player input
        rotY += xAxisInput * inputSensitivity * Time.deltaTime;
        rotX += yAxisInput * inputSensitivity * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;

        CameraSubject.transform.rotation = Quaternion.Euler(0.0f, rotY, 0.0f);
        CameraSubjectSpine.transform.rotation = Quaternion.Euler(rotX, rotY, 0.0f);
        //Prints Debug Information
        CameraDebugPrint();

    }

    void LateUpdate()
    {
        CameraUpdater();
    }

    void GetInput()
    {
        //Gets Game Controller input (if connected)
        if (Input.GetJoystickNames().Length > 0)
        {
            xAxisInput = Input.GetAxis("Right Joystick H-Axis");
            yAxisInput = Input.GetAxis("Right Joystick V-Axis");
            aimInput = Input.GetAxis("ADS Input Joystick");
        }
        //Gets Mouse Input (if Game Controller isn't connected)
        else
        {
            xAxisInput = Input.GetAxis("Mouse X");
            yAxisInput = Input.GetAxis("Mouse Y");
        }

        //Invert controls if applicable
        if (verticalInvert)
        {
            yAxisInput = -yAxisInput;
        }
        if (horizontalInvert)
        {
            xAxisInput = -xAxisInput;
        }
    }

    void CameraUpdater()
    {
        // set the target object to follow
        Transform target = CameraSubject.transform;
        var CameraTarget = target.position;

        if ((aimInput > 0) || (CameraDollyComponent.enableDebugControl && CameraDollyComponent.toggleAimingState))
        {
            CameraHOffset = Mathf.Lerp(CameraHOffset, CameraADSHorizontalOffset, cameraADSLerp);
            CameraVOffset = Mathf.Lerp(CameraVOffset, CameraADSVerticalOffset, cameraADSLerp);
            CameraFOffset = Mathf.Lerp(CameraFOffset, CameraADSForwardOffset, cameraADSLerp);
        }

        else
        {
            CameraHOffset = Mathf.Lerp(CameraHOffset, CameraHorizontalOffset, cameraADSLerp);
            CameraVOffset = Mathf.Lerp(CameraVOffset, CameraVerticalOffset, cameraADSLerp);
            CameraFOffset = Mathf.Lerp(CameraFOffset, CameraForwardOffset, cameraADSLerp);
        }

        CameraTarget += target.right * CameraHOffset;
        CameraTarget += target.up * CameraVOffset;
        CameraTarget += target.forward * CameraFOffset;

        //move towards the game object that is the target
        float step = CameraMovespeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, CameraTarget, step);
    }

    void CameraDebugPrint()
    {
        if (CameraDebugMode)
        {
            //Prints Game Controller Info if connected
            if (Input.GetJoystickNames().Length > 0)
            {
                Debug.Log("Input Mode: Game Controller",this);
                Debug.Log("X & Y Axis Input (Left Joystick): (" + xAxisInput + "," + yAxisInput + ")", this);
            }
            //Prints Mouse Info if no Game Controller Connected
            else
            {
                //Debug.Log("Input Mode: Mouse & Keyboard", this);
               // Debug.Log("X & Y Axis Input (Mouse Movement): (" + xAxisInput + "," + yAxisInput + ")", this);
                
            }
        }
        
    }

    void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, CameraTargetGizmoSize);
    }
}
