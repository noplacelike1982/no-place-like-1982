﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSystem : MonoBehaviour {

    [Header("Guns/Weapons")]
    public List<GunData> Guns = new List<GunData>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [System.Serializable]
    public class GunData
    {
        public string GunName;
        public GameObject PlayerGunObject;
        public GameObject GunProjectile;
        public AudioClip Gunshot;
        public float GunFireRate;
        public float GunKickback = 3.0f;
        public bool isAutomatic;
        [Range(-10.0f, 10.0f)]
        public float FiringPositionVerticalOffset = 0.275f;
        [Range(-10.0f, 10.0f)]
        public float FiringPositionHorizontalOffset = -0.05f;
        [Range(0.0f, 10.0f)]
        public float FiringPositionForwardOffset = 0.5f;
        public int GunDamage = 50;
        public Texture2D GunImage;  
        public string gunShotName;
    }

}
