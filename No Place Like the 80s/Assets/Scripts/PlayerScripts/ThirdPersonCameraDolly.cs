﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[HelpURL("https://docs.google.com/document/d/1mUJQYcX1A41Pbko8QDITyfAUH1ctNZ2udy_wRSrJO0s/edit?usp=sharing")]
public class ThirdPersonCameraDolly : MonoBehaviour {
    [Header("Dolly Minimum & Maximum Distance")]
    [Range(0.0f, 10.0f)]
    public float minDistance = 1.0f;
    [Range(0.0f, 10.0f)]
    public float maxDistance = 4.0f;
    [Range(0.0f, 10.0f)]
    public float zoomInDistance = 2.0f;
    [Range(0.0f, 50.0f)]
    public float dollyTrackMovementTime = 10.0f;
    Vector3 dollyDir;

    [Header("Debug Dolly Control")]
    public bool enableDebugControl = false;
    public bool toggleAimingState = false;

    //public Vector3 dollyDirAdjusted;
    private float distance;
    public float kickback = 0.0f;
    private float ads_input = 0.0f;

    // Use this for initialization
    void Awake()
    {
        dollyDir = transform.localPosition.normalized;
        distance = transform.localPosition.magnitude;
    }

    // Update is called once per frame
    void Update()
    {
        //Get Player Input
        GetInput();
        Vector3 desiredCameraPos = transform.parent.TransformPoint(dollyDir * maxDistance);
        RaycastHit hit;

        if (Physics.Linecast(transform.parent.position, desiredCameraPos, out hit))
        {
            distance = Mathf.Clamp((hit.distance * 0.87f) + kickback, minDistance, maxDistance);

        }
        else
        {
            if ((ads_input > 0) || (enableDebugControl && toggleAimingState))
            {
                distance = zoomInDistance + Random.Range(-kickback,kickback);
            }

            else
            {
                distance = maxDistance + Random.Range(-kickback, kickback);
            }
            
        }

        transform.localPosition = Vector3.Lerp(transform.localPosition, dollyDir * distance, Time.deltaTime * dollyTrackMovementTime);
        kickback = Mathf.Lerp(kickback, 0, 0.5f);
    }

    void GetInput()
    {
        ads_input = Input.GetAxis("ADS Input Joystick");
    }
}
