﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    #region
    // gloable player health var
    public static int _PlayerHealth;
    //health var viewable in inspector
    public int pHealth;
    //sets max player health
    public int maxHealth = 100;
    //var for temp health
    public int tempHealth;
    //time between decrements to temp health
    public float timeToWait = 2;
    //reference to healthbar
    public Slider healthBar;
    //reference to temphealthbar
    public Slider tempHealthBar;
    public static bool _PlayerisDead;
    #endregion

    #region
    //public set/gets

    //public method to read the players health
    public int GetPlayerHealth()
    { return _PlayerHealth; }

    public int GetTempHealth()
        { return tempHealth; }
    #endregion


    void Start ()
    {
        _PlayerHealth = 80;
	}
	
	void Update ()
    {
        pHealth = _PlayerHealth;

        healthBar.value = _PlayerHealth;

        tempHealthBar.value = tempHealth;

        if (_PlayerHealth > maxHealth)
        {
            SetTempHealth();
            StartCoroutine("TempHealthDecay");
        }

		if(_PlayerHealth <= 0)
        {
            Debug.Log("Player is Dead");
            _PlayerisDead = true;
            //uncommnet the line below to kill the player
            //Destroy(gameObject);
        }
	}

    #region
    //methods

    //public method to alter the players health
    public void ChangePlayerHealth(int amount)
    {
        _PlayerHealth += amount;
       //_PlayerHealth = Mathf.Clamp(_PlayerHealth, 0, maxHealth);

    }

    public void SetTempHealth()
    {
        int i = (_PlayerHealth - maxHealth);
        _PlayerHealth = maxHealth;
        tempHealth = i;
        i = 0;
    }

    IEnumerator TempHealthDecay()
    {
        while(tempHealth > 0)
        {
            tempHealth -= 1;

            yield return new WaitForSecondsRealtime(timeToWait);
        }
        
    }
    #endregion
}
