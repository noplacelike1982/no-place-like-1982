﻿using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //Variable Declarations
    #region
    //Public Declarations
    //Player Behavior Attributes
    [Header("Player Behavior Attributes")]
    public float PlayerSpeed = 2f; //Player Movement Speed
    public float jumpVelocity = 5f; //Player Jump Velocity

    public bool isDead = false;

    //Gunplay values
    [Header("Gunplay Behavior (Firing Position)")]
    [Range(-10.0f, 10.0f)]
    public float FirePositionVerticalOffset = 0.275f;
    [Range(-10.0f, 10.0f)]
    public float FirePositionHorizontalOffset = -0.05f;
    [Range(0.0f, 10.0f)]
    public float FirePositionForwardOffset = 0.5f;
    public GameObject muzzleFlash;

    [Header("Gunplay Behavior (Firing Behavior)")]
    public int GunID = 0;
    public GameObject GameControllerReference;
    public float MuzzleflashDelay = 0.0f;
    public GameObject CameraDolly;
    public GameObject CameraObject;

    [Header("External Objects/Component References")]
    public GameObject PlayerModel;
    public Transform SpineTransform;


    //Debug Mode
    [Header("Debug Player Controller")]
    public bool enableDebugControl = false;
    public bool drawFiringPosition = false;


    public GameObject HUD;
    public GameObject killScreen;

    //Private Declarations
    //Declare input for the character
    float horizontal_movement;
    float vertical_movement;
    float horizontal_input;
    float vertical_input;
    float fire_axis;
    float aim_axis;

    Vector3 FiringPosition;
    Transform FiringTransform;

    private bool jump_input = false;
    private bool aim_input = false;
    private bool fire_input = false;

    //Declare Gameplay Variables
    private float vertVelocity = 0f;
    private bool hasJumped;
    private bool triggerReleased = true;
    private float gunDelay = 0f;
    private float muzzleDelay = 0.0f;

    //Declare Components
    Animator PlayerAnimator;
    CharacterController player;
    CameraMount Mount;
    GunSystem Guns;
    #endregion



    // Use this for initialization
    void Start()
    {
        //Get Game Object References if not set
        #region
        if (GameControllerReference == null)
        {
            GameControllerReference = GameObject.FindGameObjectWithTag("GameController");
        }

        if (CameraDolly == null)
        {
            CameraDolly = GameObject.FindGameObjectWithTag("CameraDollyCrane");
        }

        if (CameraObject == null)
        {
            CameraObject = GameObject.FindGameObjectWithTag("MainCamera");
        }
        #endregion
        player = this.GetComponent<CharacterController>();
        PlayerAnimator = PlayerModel.GetComponent<Animator>();
        Guns = GameControllerReference.GetComponent<GunSystem>();
        Mount = CameraObject.GetComponent<CameraMount>();
        FiringTransform = SpineTransform;
        FiringPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            //Debug Calculations
            GunPositionCalculations();

            //Get Player Input
            playerInput();

            //Player Movement
            playerMovement();

            //Player Gameplay Loop
            gameplayMechanics();
        }

        if (PlayerHealth._PlayerHealth <= 0)
            KillPlayer();

    }

    void playerInput()
    {
        //Gets Gamepad input (if connected)
        if (Input.GetJoystickNames().Length > 0)
        {
            Debug.Log("gamepads connected");
            horizontal_input = Input.GetAxis("Left Joystick H-Axis");
            vertical_input = Input.GetAxis("Left Joystick V-Axis");
            jump_input = Input.GetButtonDown("Jump Input");
            fire_axis = Input.GetAxis("Fire Input Joystick");
            aim_axis = Input.GetAxis("ADS Input Joystick");

            if (fire_axis > 0)
            {
                fire_input = true;
            }
            else
            {
                fire_input = false;
            }

            if (aim_axis > 0)
            {
                CrosshairController.Aiming = true;
            }
            else
            {
                CrosshairController.Aiming = false;
            }
        }
        //Gets Mouse Input (If Gamepad not connected)
        else
        {
            Debug.Log(" no gamepads connected");
            horizontal_input = Input.GetAxisRaw("Keyboard H-Axis");
            vertical_input = Input.GetAxisRaw("Keyboard V-Axis");
            jump_input = Input.GetButtonDown("Jump Input");
            fire_axis = Input.GetAxisRaw("Fire Mouse Input");
            aim_axis = Input.GetAxisRaw("ADS Mouse Input");

            if (Input.GetMouseButtonDown(0) && !PauseScript.isPaused)
            {
                Debug.Log("Player shot");
                fire_input = true;
            }
            else
            {
                fire_input = false;
            }

            if (aim_axis > 0)
            {
                CrosshairController.Aiming = true;
            }
            else
            {
                Debug.Log("else set aiming to false");
                CrosshairController.Aiming = false;
            }
            
            if(Input.GetButtonUp("ADS Input Joystick"))
            {
                Debug.Log("button up");
                CrosshairController.Aiming = false;
            }
            
        }
    }

    void playerMovement()
    {
        //Set Horizontal and Vertical Movement
        horizontal_movement = horizontal_input * PlayerSpeed;
        vertical_movement = vertical_input * PlayerSpeed;

        //Jumping
        if (jump_input)
        {
            jump();
        }
        PlayerAnimator.SetBool("PlayerGrounded", player.isGrounded);
        //Apply Gravity
        ApplyGravity();

        //Apply Movement
        Vector3 movement = new Vector3(horizontal_movement, vertVelocity, vertical_movement);
        movement = transform.rotation * movement;

        player.Move(movement * Time.deltaTime);

        //Feed Movment Variables into 2D Blendtree
        PlayerAnimator.SetFloat("PlayerHorizontalSpeed", horizontal_movement / PlayerSpeed);
        PlayerAnimator.SetFloat("PlayerVerticalSpeed", vertical_movement / PlayerSpeed);
    }

    void gameplayMechanics()
    {
        if (Time.time > gunDelay)
        {
            if (fire_input)
            {
                muzzleFlash.SetActive(true);
                GameObject newBullet = Instantiate(Guns.Guns[GunID].GunProjectile, FiringPosition, FiringTransform.rotation) as GameObject;
                FindObjectOfType<AudioManager>().Play(Guns.Guns[GunID].gunShotName);
                gunDelay = Time.time + Guns.Guns[GunID].GunFireRate;
                Mount.screenshake += Guns.Guns[GunID].GunKickback;
                muzzleDelay = Time.time + Guns.Guns[GunID].GunFireRate + MuzzleflashDelay;
            }
        }
        else
        {
            muzzleFlash.SetActive(false);
        }

        for (int i = 0; i < Guns.Guns.Count; i++)
        {
            if (Guns.Guns[i].PlayerGunObject != null)
            {
                if (i == GunID)
                {
                    Guns.Guns[i].PlayerGunObject.SetActive(true);
                }
                else
                {
                    Guns.Guns[i].PlayerGunObject.SetActive(false);
                }
            }

        }
    }
    void jump()
    {
        if (player.isGrounded == true)
        {
            vertVelocity = jumpVelocity;
            print("Player Jumped");
            hasJumped = true;
        }
    }

    private void ApplyGravity()
    {
        if (player.isGrounded == true)
        {
            if (hasJumped == false)
            {
                vertVelocity = Physics.gravity.y;
            }
            else
            {
                vertVelocity = jumpVelocity;
            }
        }
        else
        {
            vertVelocity += Physics.gravity.y * Time.deltaTime;
            hasJumped = false;
        }

    }

    private void GunPositionCalculations()
    {
        FiringTransform = SpineTransform;
        FiringPosition = SpineTransform.position;

        FiringPosition += FiringTransform.right * FirePositionHorizontalOffset;
        FiringPosition += FiringTransform.up * FirePositionVerticalOffset;
        FiringPosition += FiringTransform.forward * FirePositionForwardOffset;
        muzzleFlash.transform.position = FiringPosition;
    }

    void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(FiringPosition, 0.01f);
    }

    void KillPlayer()
    {
        isDead = true;
        PlayerAnimator.SetBool("Dead", true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        HUD.SetActive(false);
        killScreen.SetActive(true);

        /*
        string json = this.GetComponent<LeaderBoard>().LoadCurrentBoard();
        this.GetComponent<LeaderBoard>().ParseBoard();
       // myObject.GetComponent<MyScript>().MyFunction();
       */
    }

}

[System.Serializable]
public class GunData
{
    public string GunName;
    public GameObject GunProjectile;
    public float GunFireRate;
    [Range(-10.0f, 10.0f)]
    public float FiringPositionVerticalOffset = 0.275f;
    [Range(-10.0f, 10.0f)]
    public float FiringPositionHorizontalOffset = -0.05f;
    [Range(0.0f, 10.0f)]
    public float FiringPositionForwardOffset = 0.5f;
    public int GunDam = 50;
}
