﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PauseGameController : MonoBehaviour {
    [Header("Pause Menu Contents/Functionality")]
    public List<PauseMenuData> PauseMenu = new List<PauseMenuData>();

    [Header("User Interface Settings")]
    public Font UIFont;
    [Range(0.0f, 1.0f)]
    public float PauseMenuElementSpacing;
    [Range(0.0f, 1.0f)]
    public float PauseMenuSelectorUIHeight;
    public Vector2 MenuElementDimensions;
    public Color _uiBackground;
    public Color _SelectedElementColor;
    [Range(0.0f, 1.0f)]
    public float MenuLerp;
    [Header("Pause Menu Debug Settings")]
    public bool GameIsPaused = false;
    public int PauseMenuSelector;
    public GameObject HUD;

    

    //Private Variables
    #region
    private Texture2D _staticRectTexture;
    private GUIStyle _staticRectStyle;
    private Texture2D _staticSelectedRectTexture;
    private GUIStyle _staticSelectedRectStyle;
    private float PauseSelectorPosition;
    
    #endregion
    // Use this for initialization
    void Start () {
        GameIsPaused = false;
        _staticRectTexture = new Texture2D(1, 1);
        _staticRectStyle = new GUIStyle();
        _staticRectTexture.SetPixel(0, 0, _uiBackground);
        _staticRectTexture.Apply();
        _staticRectStyle.normal.background = _staticRectTexture;

        _staticSelectedRectTexture = new Texture2D(1, 1);
        _staticSelectedRectStyle = new GUIStyle();
        _staticSelectedRectTexture.SetPixel(0, 0, _SelectedElementColor);
        _staticSelectedRectTexture.Apply();
        _staticSelectedRectStyle.normal.background = _staticSelectedRectTexture;

       
    }
	
	// Update is called once per frame
	void Update () {

        if (GameIsPaused)
        {
            //Get and Use Input
            #region
            //PC Input
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                PauseMenuSelector += 1;
                FindObjectOfType<AudioManager>().Play("mouseOver");
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                PauseMenuSelector -= 1;
                FindObjectOfType<AudioManager>().Play("mouseOver");
            }
            if ((Input.GetKeyDown(KeyCode.Space)) || (Input.GetKeyDown(KeyCode.KeypadEnter)))
            {
                if (PauseMenu[PauseMenuSelector].Action != null)
                {
                    PauseMenu[PauseMenuSelector].Action.Invoke();
                    FindObjectOfType<AudioManager>().Play("mouseClick");
                }
            }
            #endregion
            for (int i = 0; i < PauseMenu.Count; i++)
            {
                Debug.Log(PauseMenu[i]);
            }

            _staticRectTexture.SetPixel(0, 0, _uiBackground);
            _staticRectTexture.Apply();
            _staticRectStyle.normal.background = _staticRectTexture;

            _staticSelectedRectTexture.SetPixel(0, 0, _SelectedElementColor);
            _staticSelectedRectTexture.Apply();
            _staticSelectedRectStyle.normal.background = _staticSelectedRectTexture;
        }
        

        //Lerp Menu Selector
        PauseSelectorPosition = Mathf.Lerp(PauseSelectorPosition, PauseMenuSelector, MenuLerp);

        //Clamp Menu
        PauseMenuSelector = Mathf.Clamp(PauseMenuSelector, 0, PauseMenu.Count-1);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                DeactivateMenu();
            }
            else
            {
                ActivateMenu();
            }
        }
    }

    //Draw UI
    void OnGUI()
    {
        if (GameIsPaused)
        {
            //Draw Background
            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), "", _staticRectStyle);

            //Draw Menu Element Selector
            GUI.Label(new Rect(0, ((Screen.height / 2) + (PauseSelectorPosition * (Screen.height * PauseMenuElementSpacing))) - ((PauseMenuSelectorUIHeight * Screen.height) / 2), Screen.width, PauseMenuSelectorUIHeight * Screen.height), "", _staticSelectedRectStyle);
            //Draw Menu Text
            for (int i = 0; i < PauseMenu.Count; i++)
            {
                GUI.skin.font = UIFont;
                GUI.skin.label.alignment = TextAnchor.MiddleCenter;
                GUI.Label(new Rect((Screen.width / 2) - (MenuElementDimensions.x / 2), ((Screen.height / 2) + (i * (Screen.height * PauseMenuElementSpacing))) - (MenuElementDimensions.y / 2), MenuElementDimensions.x, MenuElementDimensions.y), PauseMenu[i].Name);
            }
        }
        
    }

    public void ActivateMenu()
    {
        Time.timeScale = 0;
        AudioListener.pause = true;
        GameIsPaused = true;
        //HUD.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void DeactivateMenu()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        GameIsPaused = false;
        //HUD.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void ExitGame()
    {
        SceneManager.LoadScene("Main Menu");

    }
}
[System.Serializable]
public class PauseMenuData
{
    public string Name;
    public UnityEvent Action;

}