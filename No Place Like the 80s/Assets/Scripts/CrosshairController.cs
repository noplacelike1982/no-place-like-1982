﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairController : MonoBehaviour
{
    public Texture2D crosshairImageAimed;
    public Texture2D crosshairImageHipfire;
    public Texture2D crosshairImage;
    public float Xoffset;
    public float Yoffset;
    public static bool Aiming = false;

    // Use this for initialization
    void Start ()
    {
        float xMin = (Screen.width - Input.mousePosition.x) - (crosshairImage.width / 2);
        float yMin = (Screen.height - Input.mousePosition.y) - (crosshairImage.height / 2);
    }
	
	// Update is called once per frame
	void Update ()
    {

        switch (Aiming)
        {
            case true:
                crosshairImage = crosshairImageAimed;
                break;

            case false:
                crosshairImage = crosshairImageHipfire;
                break;
        }
        float xMin = (Screen.width - Input.mousePosition.x) - (crosshairImage.width / 2);
        float yMin = (Screen.height - Input.mousePosition.y) - (crosshairImage.height / 2);

    }

    void OnGUI()
    {

            float xMin = (Screen.width / 2) - (crosshairImage.width / 2);
            float yMin = (Screen.height / 2) - (crosshairImage.height / 2);
            GUI.DrawTexture(new Rect(xMin + Xoffset, yMin + Yoffset, crosshairImage.width / 2, crosshairImage.height / 2), crosshairImage);
    }
}
