﻿using UnityEngine;
using System.IO;
using System;

public class LeaderBoard : MonoBehaviour
{
    public string json;

    //check to make sure that there is a leaderboard json file
    private void Awake()
    {

        if (!File.Exists(Application.dataPath + "/leaderboard.json"))
        {

            Debug.Log("Didnt find a leaderboard");
            Lboard newBoard = new Lboard();

            //sets newBoards scores to 1000 - 100
            for(int i = 0; i < 10; i++)
            {
                int j = 1000 - (i*100);
                newBoard.scores[i] = (j);
            }
            //sets newBoards names to Player1 - Player10
            for(int i = 0; i < 10; i++)
            {
                int j = (1 + i);
                newBoard.names[i] = ("PY"+j);
            }
            #region
            /*
            newBoard.score1 = 1000;
            newBoard.score2 = 900;
            newBoard.score3 = 800;
            newBoard.score4 = 700;
            newBoard.score5 = 600;
            newBoard.score6 = 500;
            newBoard.score7 = 400;
            newBoard.score8 = 300;
            newBoard.score9 = 200;
            newBoard.score10 = 100;

            newBoard.name1 = "Player1";
            newBoard.name2 = "Player2";
            newBoard.name3 = "Player3";
            newBoard.name4 = "Player4";
            newBoard.name5 = "Player5";
            newBoard.name6 = "Player6";
            newBoard.name7 = "Player7";
            newBoard.name8 = "Player8";
            newBoard.name9 = "Player9";
            newBoard.name10 = "Player10";
            */
            #endregion

            UpdateBoard(newBoard);
        }
        else
        {
            Debug.Log("Found a leaderboard");
            json = File.ReadAllText(Application.dataPath + "/leaderboard.json");
            LoadCurrentBoard(json);
        }
    }

    private void Update()
    {
        //input to make the game update the score board
        if (PlayerHealth._PlayerisDead)
        {
            Debug.Log("updating board");
            ParseBoard(json);
        }
    }

    //leaderboard constructor
    [Serializable]
    public class Lboard
    {
        public int[] scores = new int[10];

        public string[] names = new string[10];
    }

    public void LoadCurrentBoard(string json)
    {
        //load the myboard from the json string we just saved and print it
        Lboard loadedBoard = JsonUtility.FromJson<Lboard>(json);
        Debug.Log("1st: " + loadedBoard.names[0] + "\t" + loadedBoard.scores[0]
                    + "\n2nd: " + loadedBoard.names[1] + "\t" + loadedBoard.scores[1]
                    + "\n3rd: " + loadedBoard.names[2] + "\t" + loadedBoard.scores[2]
                    + "\n4th: " + loadedBoard.names[3] + "\t" + loadedBoard.scores[3]
                    + "\n5th: " + loadedBoard.names[4] + "\t" + loadedBoard.scores[4]
                    + "\n6th: " + loadedBoard.names[5] + "\t" + loadedBoard.scores[5]
                    + "\n7th: " + loadedBoard.names[6] + "\t" + loadedBoard.scores[6]
                    + "\n8th: " + loadedBoard.names[7] + "\t" + loadedBoard.scores[7]
                    + "\n9th: " + loadedBoard.names[8] + "\t" + loadedBoard.scores[8]
                    + "\n10th: " + loadedBoard.names[9] + "\t" + loadedBoard.scores[9]);
    }

    private void UpdateBoard(Lboard newScore)
    {
        //covert myBoard into a json string and print the class
        string json = JsonUtility.ToJson(newScore);
        Debug.Log(json);

        File.WriteAllText(Application.dataPath + "/leaderboard.json", json);
    }

    public void ParseBoard(string jsonToParse)
    {
        //var for the players current score
        int playerScore = HighScore.highScore;

        //var holding the location of the highscore the player just beat
        int indexPlace = 10;

        string json1;

        //pulls the leaderboard.json file into a new Lboard class
        bool change = false;

        //bool to trigger changing the leaderboard
        Lboard tempBoard = JsonUtility.FromJson<Lboard>(jsonToParse);
        Debug.Log("Checking To update board");

        //checks all memebers of the leaderboard agaisnt the players score, breaking if it does and returning the index location of the beaten score
        for(int i = 0; i < 10; i++)
        {
            if(tempBoard.scores[i] <= playerScore)
            {
                change = true;
                indexPlace = i;
                Debug.Log("You beat a high score " + i);
                break;
            }
        }

        //conditional when triggered updates the leaderboard to include the players new score
        if (change)
        {
            //moves all highscore values down one place
            for(int i = 9; i == indexPlace; i--)
            {
                tempBoard.scores[i] = tempBoard.scores[i - 1];
                tempBoard.names[i] = tempBoard.names[i - 1];
            }

            //inputs the players score in the leaderboard
            tempBoard.scores[indexPlace] = playerScore;
            tempBoard.names[indexPlace] = StaticPlayerRefs.PlayerName;

            json1 = JsonUtility.ToJson(tempBoard);

            if (File.Exists(Application.dataPath + "/leaderboard.json"))
            {
                Debug.Log("found a leader board to delete");
                File.Delete((Application.dataPath + "/leaderboard.json"));
                File.WriteAllText(Application.dataPath + "/leaderboard.json", json1);
            }
            else
            {
                File.WriteAllText(Application.dataPath + "/leaderboard.json", json1);
                Debug.Log("didn't find a leaderboard to delete");
            }
            Debug.Log(json1);
            change = false;
        }
    }
}
