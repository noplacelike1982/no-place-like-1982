﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorsoCollider : MonoBehaviour
{
    public Enemy thisZombie;

    public int damageToDeal = 25;

    public int scorePoints = 25;

    // Use this for initialization

    void Start()
    {
        thisZombie = GetComponentInParent<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        Debug.Log("The torso touched something");
        if ((other.gameObject.tag == "Bullet") && (thisZombie.ZombieHealth > 0))
        {
            Debug.Log("I got shot in the torso");
            thisZombie.zombieHealth -= damageToDeal;
            Debug.Log("Damage Dealt = " + damageToDeal);

            ScoreTotal.scoreTotal += scorePoints;
            HighScore.highScore += scorePoints;
        }
        else
            return;
    }

}