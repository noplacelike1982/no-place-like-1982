﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimbHit : MonoBehaviour
{
    public Enemy thisZombie;

    public int damageToDeal = 10;

    public int scorePoints = 10;

    // Use this for initialization

    void Start()
    {
        thisZombie = GetComponentInParent<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        Debug.Log("The arm touched something");
        if (thisZombie.ZombieHealth > 0)
        {
            if (other.gameObject.tag == "Bullet" && !PickUpSystem._DoublePoints && !PickUpSystem._TriplePoints)
            {
                Debug.Log("I got shot in the limb");
                thisZombie.zombieHealth -= damageToDeal;
                Debug.Log("Damage Dealt = " + damageToDeal);

                ScoreTotal.scoreTotal += scorePoints;
                HighScore.highScore += scorePoints;
            }
            else if (other.gameObject.tag == "Bullet" && PickUpSystem._DoublePoints)
            {
                Debug.Log("I got shot in the limb DOUBLE POINTS");
                thisZombie.zombieHealth -= damageToDeal;
                Debug.Log("Damage Dealt = " + damageToDeal);

                ScoreTotal.scoreTotal += (scorePoints * 2);
                HighScore.highScore += (scorePoints * 2);
            }
            else if (other.gameObject.tag == "Bullet" && PickUpSystem._TriplePoints)
            {
                Debug.Log("I got shot in the limb TRIPLE POINTS");
                thisZombie.zombieHealth -= damageToDeal;
                Debug.Log("Damage Dealt = " + damageToDeal);

                ScoreTotal.scoreTotal += (scorePoints * 3);
                HighScore.highScore += (scorePoints * 3);
            }
        }
        else
            return;
    }

}
