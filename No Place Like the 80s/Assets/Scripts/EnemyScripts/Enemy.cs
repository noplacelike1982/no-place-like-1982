﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    NavMeshAgent agent;
    Transform objectToChase;
    public GameObject ZombieRenderObject;
    public float DeathTimer;
    private float Timer;

    Animator ZombieAnimator;
    //GameObject thisCollider;
    //public Collider hitBox;
    
    //reference to the players gun
    GunData gundata = new GunData();

    int spawnPoint;
    public int zombieHealth = 100;
    public float speed;
    public bool tracking = true;
    public bool madeCan = false;
    public bool attackedCheck = false;
    public float attackCool = 3.0f;
    public int damage = 20;


    //references (player, scoreboard)
    private GameObject player;
    private PickUpSystem pick;

    //functions for zombie health
    public int ZombieHealth
    {
        get
        {
            return zombieHealth;
        }

        set
        {
            zombieHealth = value;
        }
    }

    
    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        ZombieAnimator = ZombieRenderObject.GetComponent<Animator>();
        objectToChase = player.transform;
        pick = player.GetComponent<PickUpSystem>();
        

        agent = GetComponent<NavMeshAgent>();

        //dev tool to make them not chase you while testing
        if(tracking == true)
        agent.SetDestination(objectToChase.position);
		
	}
	
	// Update is called once per frame
	void Update () {

        //dev tool to make them not chase you while testing
        if ((tracking == true) && (agent.enabled))
        { agent.SetDestination(objectToChase.position); }

        ZombieAnimator.SetFloat("zombieSpeed", agent.velocity.magnitude / agent.speed);

        CheckDeath();

        float distance = Vector3.Distance(objectToChase.position, transform.position);

        if(distance < 2.0f && !attackedCheck)
        {
            Debug.Log("Attacking");
            ZombieAnimator.SetBool("Attack", true);
            attackedCheck = true;
            attackCool = 3.0f;
        }

        if (attackCool > 0)
        {
            attackCool -= Time.deltaTime;
            Debug.Log("On cooldown");
        }
        else
        {
            Debug.Log("off cooldown");
            attackedCheck = false;
            attackCool = 3.0f;
        }


    }

    public void AttackEnd()
    {
        if (Vector3.Distance(objectToChase.position, transform.position) < 2.0f)
        {
            PlayerHealth._PlayerHealth -= damage;
        }
        ZombieAnimator.SetBool("Attack", false);
    }

    //finds the player and sets a destination towards them using the navmesh
    void SetDestination()
    {
        if (agent.enabled)
        {
            agent.SetDestination(objectToChase.position);
        }
        
    }


    void CheckDeath()
    {
        if (this.zombieHealth <= 0)
        {
            if (!ZombieAnimator.GetBool("isDead"))
            {
                ZombieAnimator.SetBool("isDead", true);
            }

            agent.speed = 0;
            agent.enabled = false;
            gameObject.tag = "DeadEnemy";
            if (!madeCan)
            {
                pick.MakeCan(transform);
                Timer = Time.time + (DeathTimer * Time.deltaTime);
                madeCan = true;
            }
            this.GetComponentInChildren<CapsuleCollider>().enabled = false;

            if (Timer < Time.time)
            {
                Destroy(gameObject);
            }
            
        }
    }
}

