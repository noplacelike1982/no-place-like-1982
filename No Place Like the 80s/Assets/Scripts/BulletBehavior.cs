﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour {

    [Range(0.0f, 100.0f)]
    public float BulletSpeed = 1.0f;
    Rigidbody BulletPhysics;
    public Vector3 PlayerPosition;
    public float MaxLength;
	// Use this for initialization
	void Start () {
        BulletPhysics = GetComponent<Rigidbody>();
        BulletPhysics.velocity = (transform.forward * (BulletSpeed));
        PlayerPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (Vector3.Distance(transform.position,PlayerPosition) > MaxLength)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
