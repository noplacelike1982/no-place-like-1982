﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    //array to hold our sounds in
    public Sounds[] sounds;
    
    //creates audio sources for all the memebers of the sounds array
	void Awake ()
    {
        DontDestroyOnLoad(this);

        foreach(Sounds s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }
		
	}
	
    //method to play sound 
	public void Play (string name)
    {
        Sounds s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }
}
