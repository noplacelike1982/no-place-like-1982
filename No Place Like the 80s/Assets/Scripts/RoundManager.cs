﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundManager : MonoBehaviour
{
    public int currentRound;
    public int nextRound;
    public int numberOfZombies;
    public float gameTimer;
    public int seconds = 0;



	// Use this for initialization
	void Start () {

        gameTimer = Time.time;
	}
	
	// Update is called once per frame
	void Update () {

        IntroTimer();

       // if(currentRound == nextRound)
        

	}

    public void IntroTimer()
    {
        // pre round 1 timer logic
        if (Time.time > gameTimer + 1 && currentRound < 1)
        {
            gameTimer = Time.time;
            seconds++;
            Debug.Log(seconds);
        }

        //once the first round starts the timer stops
        if (seconds == 5 && currentRound < 1)
        {
            currentRound = 1;
            nextRound = 2;
        }
    }
}
