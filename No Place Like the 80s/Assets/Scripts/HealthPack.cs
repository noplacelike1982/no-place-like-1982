﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour
{

    public int healthToHeal = 25;

    PlayerHealth health = new PlayerHealth();

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("You touched a health pack");

        if (other.tag == "Player")
        {
            Debug.Log("I healed the player");

            health.ChangePlayerHealth(healthToHeal);

            Destroy(gameObject);
        }
    }

}
