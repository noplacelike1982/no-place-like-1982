﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour
{
    public GameObject leaderPanel;
    public GameObject startMenu;
    public GameObject enterName;
    public GameObject title;
    public InputField playerName;
    public GameObject gameModeSelect;
    public GameObject levelSelect;

    public static string _LevelChoice;


    private void Awake()
    {
        leaderPanel.SetActive(false);
        enterName.SetActive(false);
        gameModeSelect.SetActive(false);
        levelSelect.SetActive(false);
        title.SetActive(true);
        startMenu.SetActive(true);

        _LevelChoice = "ArcadeLevelFinal";
    }

    public void PlayGame()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        StaticPlayerRefs.PlayerName = playerName.text;
        SceneManager.LoadScene(_LevelChoice);
    }

    public void OpenLeaderboard()
    {
        startMenu.SetActive(false);
        title.SetActive(false);
        leaderPanel.SetActive(true);
    }

    public void CloseLeaderboard()
    {
        leaderPanel.SetActive(false);
        gameModeSelect.SetActive(false);
        title.SetActive(true);
        startMenu.SetActive(true);

    }

    public void OpenEnterName()
    {
        startMenu.SetActive(false);
        title.SetActive(false);
        gameModeSelect.SetActive(false);
        enterName.SetActive(true);
    }

    public void CloseEnterName()
    {
        enterName.SetActive(false);
        title.SetActive(true);
        gameModeSelect.SetActive(true);
        _LevelChoice = "ArcadeLevelFinal";
    }

    public void OpenLevelSelect()
    {
        title.SetActive(false);
        gameModeSelect.SetActive(false);
        levelSelect.SetActive(true);
    }

    public void CloseLevelSelect()
    {
        levelSelect.SetActive(false);
        gameModeSelect.SetActive(true);
        _LevelChoice = "ArcadeLevelFinal";
        title.SetActive(true);
    }

    public void LevelName_1()
    {
        levelSelect.SetActive(false);
        enterName.SetActive(true);
    }

    public void LevelName_2()
    {
        levelSelect.SetActive(false);
        enterName.SetActive(true);
        _LevelChoice = "DanceClub";
    }

    public void LevelName_3()
    {
        levelSelect.SetActive(false);
        enterName.SetActive(true);
        _LevelChoice = "VideoStoreLevel";
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void ExitGame()
    {
        SceneManager.LoadScene(0);
    }

    public void StartGame()
    {
        startMenu.SetActive(false);
        gameModeSelect.SetActive(true);

    }



}