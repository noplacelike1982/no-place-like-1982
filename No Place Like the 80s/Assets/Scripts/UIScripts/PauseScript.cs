﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PauseScript : MonoBehaviour
{
    //[SerializeField] private GameObject pauseMenuUI;

    [SerializeField] public static bool isPaused;

    public GameObject HUD;

    
    private void Start()
    {
    //Get Game Object References
    HUD = GameObject.FindGameObjectWithTag("HUD");
    }
    private void Update()
    {
        if (Input.GetButtonDown("Pause")) 
        {
            isPaused = !isPaused;
        }

        if (isPaused)
        {
            ActivateMenu();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        else
        {
            DeactivateMenu();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    void ActivateMenu()
    {
        HUD.SetActive(false);
        Time.timeScale = 0;
        AudioListener.pause = true;
       // pauseMenuUI.SetActive(true);
    }

    public void DeactivateMenu()
    {
 
        Time.timeScale = 1;
        AudioListener.pause = false;
        //pauseMenuUI.SetActive(false);
        isPaused = false;
    }

    public void ExitGame()
    {
        SceneManager.LoadScene("Main Menu");
    }
}


