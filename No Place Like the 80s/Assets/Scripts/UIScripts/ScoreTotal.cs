﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreTotal : MonoBehaviour
{

    //static variable to read in the score easily from anywhere
    public static int scoreTotal;

    public Text scoreTotalText;

    
    void Awake()
    {
        scoreTotal = 0;
    }

	
	
	void Update ()
    {
        scoreTotalText.text = scoreTotal.ToString();
	}
}
