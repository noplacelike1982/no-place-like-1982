﻿using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour
{

    //score
    public static int highScore;

    public Text highScoreText;

    // Use this for initialization
    void Awake()
    {
        highScore = 0;
    }


    // Update is called once per frame
    void Update()
    {

        highScoreText.text = highScore.ToString();
    }
}
