﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseWeapon : MonoBehaviour {

    private float ActivationDistance = 1.5f;
    private float CurrentDistance;
    public int PurchaseAmount;
    public int GunID;
    public GameObject Player;
    public GameObject GameController;
    private GunSystem Guns;
    private PlayerController PlayerAttributes;
    private bool GunBought;
    public Font UIFont;

    private int TextWidth = 256;
    private int TextHeight = 64;

    private string InteractGamepad = "X Button";
    private string InteractKeyboard = "E Key";

    // Use this for initialization
    void Start () {
        Player = GameObject.FindGameObjectWithTag("Player");
        GameController = GameObject.FindGameObjectWithTag("GameController");
        PlayerAttributes = Player.GetComponent<PlayerController>();
        Guns = GameController.GetComponent<GunSystem>();
	}
	
	// Update is called once per frame
	void Update () {

        CurrentDistance = Vector3.Distance(transform.position, Player.transform.position);

        if ((CurrentDistance <= ActivationDistance) && (!GunBought))
        {
            if (GunBought == false)
            {
                if ((Input.GetButtonDown("Pickup Input")) && (ScoreTotal.scoreTotal >= PurchaseAmount))
                {
                    ScoreTotal.scoreTotal -= PurchaseAmount;
                    PlayerAttributes.GunID = GunID;
                    GunBought = true;
                }
            }

            else
            {
                PlayerAttributes.GunID = GunID;
            }

           

        }
    }

    void OnDrawGizmosSelected()
    {
        // Display the interect radius when selected
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, ActivationDistance);
    }

    void OnGUI()
    {
        if (CurrentDistance <= ActivationDistance)
        {
            if (GunBought == false)
            {
                GUI.skin.font = UIFont;
                GUI.Label(new Rect((Screen.width / 2) - (TextWidth / 2), ((Screen.height / 4) * 3) - (TextHeight / 2), TextWidth, TextHeight), "Press " + InteractKeyboard + " to purchase " + Guns.Guns[GunID].GunName + " for " + PurchaseAmount.ToString());
            }

            else
            {
                GUI.skin.font = UIFont;
                GUI.Label(new Rect((Screen.width / 2) - (TextWidth / 2), ((Screen.height / 4) * 3) - (TextHeight / 2), TextWidth, TextHeight),"Press " + InteractKeyboard + " to equip " + Guns.Guns[GunID].GunName);
            }
            
        }

    }
}
