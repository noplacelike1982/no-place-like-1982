﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticPlayerRefs
{
    private static string _PlayerName;

    public static string PlayerName
    {
        get
        {
            return _PlayerName;
        }
        set
        {
            _PlayerName = value;
        }
    }
	
}
