using UnityEngine;
using System.Collections;

public class DanceFloor_RandomColours : MonoBehaviour {
	
public int uvAnimationTileX = 8; 
 
public int uvAnimationTileY = 8; 

private float framesPerSecond = 1.0f;

private int currentTimeInt = 0;
private int randomNumX;
private int randomNumY;
private Vector2 offsetSquare;

private Renderer _myRenderer;

void Start () 
{
		_myRenderer = GetComponent<Renderer>();
		if(_myRenderer == null)
		enabled = false;

		framesPerSecond = DanceFloor_ChangeSpeed.animSpeed;
}


void Update (){

	float index = Time.time * framesPerSecond;

	if (index > currentTimeInt) 
	{
	currentTimeInt = (int)index;
	currentTimeInt = currentTimeInt + 1;

    randomNumX = (1 * (Random.Range (1, uvAnimationTileX)));
	randomNumY = (1 * (Random.Range (1, uvAnimationTileY)));
	}

	Vector2 size = new Vector2 (1.0f / uvAnimationTileX, 1.0f / uvAnimationTileY);
	
	Vector2	offsetSquare = new Vector2 ( 1.0f / uvAnimationTileX  * randomNumX, 1.0f / uvAnimationTileY * randomNumY );
	
	_myRenderer.material.SetTextureOffset ("_MainTex", offsetSquare);
	_myRenderer.material.SetTextureScale ("_MainTex", size);

}
}