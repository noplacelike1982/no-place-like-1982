﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{

    public Text infoText;

    public string json;

    public GameObject score;
    public GameObject button;

    [Serializable]
    public class Lboard
    {
        public int[] scores = new int[10];

        public string[] names = new string[10];
    }


    public void ReadText()
    {
        button.SetActive(false);
        score.SetActive(true);

        //instantiate a new lboard
        Lboard loadedBoard = new Lboard();
        //set json to our leaderboard.json file
        json = File.ReadAllText(Application.dataPath + "/leaderboard.json");
        //initialize myboard with our leaderboard.json values
        loadedBoard = JsonUtility.FromJson<Lboard>(json);

        //console version
        /*
        Debug.Log("1st: \t\t\t" + loadedBoard.names[0] + "\t\t\t\t" + loadedBoard.scores[0]
                    + "\n2nd: " + loadedBoard.names[1] + "\t" + loadedBoard.scores[1]
                    + "\n3rd: " + loadedBoard.names[2] + "\t" + loadedBoard.scores[2]
                    + "\n4th: " + loadedBoard.names[3] + "\t" + loadedBoard.scores[3]
                    + "\n5th: " + loadedBoard.names[4] + "\t" + loadedBoard.scores[4]
                    + "\n6th: " + loadedBoard.names[5] + "\t" + loadedBoard.scores[5]
                    + "\n7th: " + loadedBoard.names[6] + "\t" + loadedBoard.scores[6]
                    + "\n8th: " + loadedBoard.names[7] + "\t" + loadedBoard.scores[7]
                    + "\n9th: " + loadedBoard.names[8] + "\t" + loadedBoard.scores[8]
                    + "\n10th: " + loadedBoard.names[9] + "\t" + loadedBoard.scores[9]);
        */
        infoText.text = ("Your Score \n" + StaticPlayerRefs.PlayerName + "------" + HighScore.highScore);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
