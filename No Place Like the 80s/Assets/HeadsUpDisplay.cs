﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadsUpDisplay : MonoBehaviour {
    //Variable Declarations
    [Header("User Interface General Settings")]
    public List<UIContainer> UIContainerElement = new List<UIContainer>();

    [Header("Highscore Element Settings")]
    public Vector2 highscoreUIPosition;
    public Font highscoreFont;
    public int highscoreFontSize;
    public string highscorePretext;
    public string highscorePosttext;
    public Color highscoreTextColor;
    public Vector2 highscoreIconUIPosition;
    public Vector2 highscoreIconUIResolution;
    public Texture2D highscoreIcon;

    private GUIStyle highscoreElementStyle = new GUIStyle();

    [Header("Currency Element Settings")]
    public Vector2 currencyUIPosition;
    public Font currencyFont;
    public int currencyFontSize;
    public string currencyPretext;
    public string currencyPosttext;
    public Color currencyTextColor;
    public Vector2 currencyIconUIPosition;
    public Vector2 currencyIconUIResolution;
    public Texture2D currencyIcon;

    private GUIStyle currencyElementStyle = new GUIStyle();

    [Header("Health Element Settings")]
    public Vector2 healthUIPosition;
    public Font healthFont;
    public int healthFontSize;
    public string healthPretext;
    public string healthPosttext;
    public Color healthTextColor;
    public Vector2 healthIconUIPosition;
    public Vector2 healthIconUIResolution;
    public Texture2D healthIcon;

    private GUIStyle healthElementStyle = new GUIStyle();

    [Header("Debug Options")]
    public bool debugMode;
    public bool drawDebugGrid;
    public Color debugGridColor;
    public Vector2 gridDimensions;

    private Texture2D _staticLineTexture;
    private GUIStyle _staticLineStyle;

    private Texture2D currencyTexture;
    private Texture2D highscoreTexture;

    private Texture2D currencyShadowTexture;
    private Texture2D highscoreShadowTexture;
    // Initialization
    void Start () {
        _staticLineTexture = new Texture2D(1, 1);
        _staticLineStyle = new GUIStyle();
        _staticLineTexture.SetPixel(0, 0, debugGridColor);
        _staticLineTexture.Apply();
        _staticLineStyle.normal.background = _staticLineTexture;

        currencyTexture = new Texture2D(1, 1);
        highscoreTexture = new Texture2D(1, 1);

        currencyShadowTexture = new Texture2D(1, 1);
        highscoreShadowTexture = new Texture2D(1, 1);
}
	
	// Update Loop
	void Update () {

        //Set Highscore Element Style
        highscoreElementStyle.fontSize = highscoreFontSize;
        highscoreElementStyle.alignment = TextAnchor.MiddleLeft;
        highscoreElementStyle.font = highscoreFont;
        highscoreElementStyle.normal.textColor = highscoreTextColor;


        //Set Currency Element Style
        currencyElementStyle.fontSize = currencyFontSize;
        currencyElementStyle.alignment = TextAnchor.MiddleLeft;
        currencyElementStyle.font = currencyFont;
        currencyElementStyle.normal.textColor = currencyTextColor;
    }

    // Draws to GUI Layer
    void OnGUI()
    {


    //Draw Grid if active
    if (drawDebugGrid)
        {
        for (int i = 0; i < gridDimensions.x; i++)
            {
                GUI.Label(new Rect((Screen.width/gridDimensions.x) * i, 0, 2, Screen.height), "", _staticLineStyle);
            }

            for (int i = 0; i < gridDimensions.y; i++)
            {
                GUI.Label(new Rect(0, (Screen.height / gridDimensions.y) * i, Screen.width, 2), "", _staticLineStyle);
            }
        }
        //Draw UI Container
        for(int i = 0; i < UIContainerElement.Count; i++)
        {
            GUI.color = UIContainerElement[i].UIElementColor;
            GUI.DrawTexture(new Rect((Screen.width / gridDimensions.x) * UIContainerElement[i].UIContainerLocation.x, (Screen.height / gridDimensions.y) * UIContainerElement[i].UIContainerLocation.y, UIContainerElement[i].UIContainerDimensions.x, UIContainerElement[i].UIContainerDimensions.y), UIContainerElement[i].UITexture);
            GUI.color = Color.white;
        }
        

        //Draw Scores
        GUI.Label(new Rect((Screen.width / gridDimensions.x) * highscoreUIPosition.x, (Screen.height / gridDimensions.y) * highscoreUIPosition.y, 256, 64), highscorePretext + HighScore.highScore.ToString() + highscorePosttext, highscoreElementStyle);
        GUI.DrawTexture(new Rect((Screen.width / gridDimensions.x) * highscoreIconUIPosition.x, (Screen.height / gridDimensions.y) * highscoreIconUIPosition.y, highscoreIconUIResolution.x, highscoreIconUIResolution.y), highscoreIcon);
        

        GUI.Label(new Rect((Screen.width / gridDimensions.x) * currencyUIPosition.x, (Screen.height / gridDimensions.y) * currencyUIPosition.y, 256, 64),currencyPretext + ScoreTotal.scoreTotal.ToString() + currencyPosttext, currencyElementStyle);
        GUI.DrawTexture(new Rect((Screen.width / gridDimensions.x) * currencyIconUIPosition.x, (Screen.height / gridDimensions.y) * currencyIconUIPosition.y, currencyIconUIResolution.x, currencyIconUIResolution.y), currencyIcon);
    }

    [System.Serializable]
    public class UIContainer
    {
        public Texture2D UITexture;
        public Vector2 UIContainerLocation;
        public Vector2 UIContainerDimensions;
        public Color UIElementColor;
    }
}
