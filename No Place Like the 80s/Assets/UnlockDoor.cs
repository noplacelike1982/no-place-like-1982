﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockDoor : MonoBehaviour {

    //
    public GameObject Player;
    public float ActivationDistance = 1.0f;
    private float CurrentDistance;
    public int PurchaseWall;
    private Transform PlayerTransform;
    private bool DoorOpened;
    public Font UIFont;

    public int TextWidth;
    public int TextHeight;

    public string InteractGamepad;
    public string InteractKeyboard;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        CurrentDistance = Vector3.Distance(transform.position, Player.transform.position);

        if ((CurrentDistance <= ActivationDistance) && (!DoorOpened))
        {
        

        if ((Input.GetButtonDown("Pickup Input")) && (ScoreTotal.scoreTotal >= PurchaseWall))
            {
                ScoreTotal.scoreTotal -= PurchaseWall;
                Destroy(gameObject);
            }
            
        }
	}

    void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, ActivationDistance);
    }

    void OnGUI()
    {
        if ((CurrentDistance <= ActivationDistance) && (!DoorOpened))
        {
            GUI.skin.font = UIFont;
            GUI.Label(new Rect((Screen.width / 2) - (TextWidth / 2), ((Screen.height/4)*3) - (TextHeight / 2), TextWidth, TextHeight), InteractKeyboard);
        }
            
    }
}
